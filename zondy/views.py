from django.http import HttpResponse
from django.shortcuts import render
from Cython.Build import cythonize
from setuptools import setup
from ColorIt import *
import sys, os, json
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def post(request):
    if request.method == 'POST':
        code = list(request.POST.items())
        # code = json.loads(code)['code']
        print(code)
    return HttpResponse('OK')


def start_this(request):

    file = open(r'C:\Users\Rob\Desktop\site\mysite\py\zzyx.py')
    code = file.read()

    return render(request, 'z.html', {'code': code})


def ensure(pth):
    if not os.path.isdir(pth):
        os.makedirs(pth)
    return pth


def main(request, path):

    # split = path.split('%A7')
    # fn = split.pop()
    # np = split[0]
    # new_path = np[:np.rfind('\\')] + f'\\{fn}'
    # print.green(new_path)
    #
    # return HttpResponse('OK')
    #
    # '----------------------------------------------------'

    sys.argv = ['', 'build_ext', '--inplace']
    sys.stdout = open(os.devnull, 'w')

    split = path.split('\\')
    file = split.pop()
    file = file[:file.rfind('.')]

    folder = '\\'.join(split)
    pyd = ensure(f'{folder}\\pyd')
    f_file = f'{folder}\\{file}'
    pyd_file = f'{pyd}\\{file}'

    setup(
        ext_modules = cythonize(path, language_level='3')
    )

    os.remove(f'{f_file}.c')
    os.replace(
        f'{f_file}.cp37-win_amd64.pyd', f'{pyd_file}.pyd'
    )

    return HttpResponse('OK')
