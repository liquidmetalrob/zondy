if (0 && code.search(/\bint\b/) > -1) {
  code = 'from cython import int\n' + code
}

code = code.replace(/\bdef\b +(\w+)\(([^\)]*)\)/g, function(x, a,b,c,d) {
  var split = b.split(',')
  for (e of split) {
    console.log(e.trim())
  }

  return `cpdef ${a}(${b})`
})

console.log(code)

fetch('/post', {
  method: 'POST',
  headers: {
    'Content-Type':'application/json'
  },
  body: JSON.stringify({'code': `${code}`})
})
