from django.urls import path
from zondy import views

urlpatterns = [
    path('', views.start_this),
    path('post', views.post),
    # path('<path>', views.main),
]
